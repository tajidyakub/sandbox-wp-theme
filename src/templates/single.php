<?php get_header(); ?>

<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            set_query_var('post_id', get_the_ID());
            get_template_part('partials/post', 'titlehead');
        }
    }
?>

<?php get_footer(); ?>
