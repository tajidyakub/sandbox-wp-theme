<?php
$post_id = get_query_var('post_id', false);

if ($post_id) {
    $post_title     = get_the_title($post_id);
    $post_subtitle  = get_field('subtitle', $post_id);
    $post_pub_date  = get_the_time($post_id);
    $post_category  = get_the_category($post_id);
    $post_tags      = get_the_tags($post_id);
?>
    <div class="container">
        <div class="row">
            <div class="col py-3 px-sm-4 px-md-5">
                <h1 class="display4"><?php echo $post_title; ?></h1>
            </div>
        </div>
    </div>
<?php
} // endif post_id
?>
