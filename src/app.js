import $ from 'jquery'
import 'popper.js'
import 'bootstrap'
import './assets/scss/style.scss'

window.$ = window.jQuery = $
