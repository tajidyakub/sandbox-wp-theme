const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: 'development',
  devServer: {
    port: 23456,
    contentBase: './dist'
  },
  devtool: 'inline-source-map',
  entry: {
    app: './src/app.js'
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new ExtractTextPlugin({
      filename:  (getPath) => {
        if (getPath('[name]') === 'app') return 'style.css'
        else return getPath('css/[name].css').replace('css/js', 'css')
      },
      allChunks: true
    }),
    new CopyWebpackPlugin([
      { from: 'src/templates/*.*', to: '[name].[ext]', toType: 'template' }
    ]),
    new CopyWebpackPlugin([
      { from: 'src/templates/partials/*.*', to: 'partials/[name].[ext]', toType: 'template' }
    ]),
    new CopyWebpackPlugin([
      { from: 'src/templates/langs/*.*', to: 'langs/[name].[ext]', toType: 'template' }
    ]),
    new CopyWebpackPlugin([
      { from: 'src/assets/icns/*.*', to: 'icns/[name].[ext]', toType: 'template' }
    ]),
    new CopyWebpackPlugin([
      { from: 'src/assets/css/*.*', to: 'css/[name].[ext]', toType: 'template' }
    ]),
    new CopyWebpackPlugin([
      { from: 'src/assets/webfonts/*.*', to: 'webfonts/[name].[ext]', toType: 'template' }
    ])

  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: { presets: ['env'] }
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract(
          {
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: { sourceMap: true }
              },
              {
                loader: 'sass-loader',
                options: { sourceMap: true }
              }
            ]
          })
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract(
          {
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: { sourceMap: true }
              },
              {
                loader: 'postcss-loader',
                options: {
                  config: {
                    ctx: {
                      'postcss-preset-env': {},
                      cssnano: {},
                    }
                  }
                }
              }
            ]
          })
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'imgs/[name].[ext]'
            }
          }
        ]
      }
    ]
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'scripts/[name].js'
  }
}
