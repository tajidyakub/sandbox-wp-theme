<header>
    <nav id="mainNavbar" class="navbar navbar-dark bg-dark navbar-expand-sm main-navbar py-0">
        <div class="container">
            <div class="navbar-collapse collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-0 mr-auto">
                    <li class="nav-item">
                        <a href="/" class="nav-link text-uppercase">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link text-uppercase">Frontend</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link text-uppercase">Serverside</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link text-uppercase">Sysadmin</a>
                    </li>
                    <li class="nav-item">
                        <a href="/" class="nav-link text-uppercase">About</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
